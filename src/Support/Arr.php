<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Support;

/**
 * This code use a some parts of the Illiminate\Support\Arr librairy which is
 * a part of the Laravel project.
 */
class Arr
{
    /**
     * Determines if an array is associative.
     *
     * An array is "associative" if it doesn't have sequential numerical
     * keys beginning with zero.
     *
     * @param array<int|string, mixed> $array
     */
    public static function isAssoc(array $array): bool
    {
        $keys = array_keys($array);

        return array_keys($keys) !== $keys;
    }

    /**
     * Determines if an array is a list.
     *
     * An array is a "list" if all array keys are sequential integers
     * starting from 0 with no gaps in between.
     *
     * @param array<int|string, mixed> $array
     */
    public static function isList(array $array): bool
    {
        return ! self::isAssoc($array);
    }
}
