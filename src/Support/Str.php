<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Support;

class Str
{
    public static function contains(string $needle, string $haystack): bool
    {
        return false !== strpos($haystack, $needle);
    }

    public static function startsWith(string $needle, string $haystack): bool
    {
        return 0 === strpos($haystack, $needle);
    }

    public static function before(string $needle, string $haystack): string
    {
        if (false === ($index = strpos($haystack, $needle))) {
            return '';
        }

        return substr($haystack, 0, $index);
    }

    public static function after(string $needle, string $haystack): string
    {
        if (false === ($index = strpos($haystack, $needle))) {
            return '';
        }

        return substr($haystack, $index + strlen($needle));
    }
}
