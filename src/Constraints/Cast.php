<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Constraints;

use Nolikein\Objectable\Contracts\CastPattern;
use Nolikein\Objectable\Contracts\Constraint;

abstract class Cast implements Constraint, CastPattern
{
    /**
     * Retrieve the constraint name.
     */
    public function getConstraintName(): string
    {
        return 'casting';
    }

    /**
     * Checks if a variable can be casted by the caster.
     */
    public function allows(mixed $value): bool
    {
        return $this->canCast($value);
    }
}
