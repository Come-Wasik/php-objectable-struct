<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Constraints;

use Nolikein\Objectable\Contracts\Constraint;

class Instance implements Constraint
{
    /** @var string The class that the instance should be */
    protected string $class = '';

    /** @var bool Is the instance nullable ? */
    protected bool $nullable = true;

    public function __construct(string $class)
    {
        $this->class = $class;
    }

    /**
     * Create a new "instance of" from class.
     */
    public static function of(string $class): self
    {
        return new self($class);
    }

    /**
     * Create a new "instance of" from object.
     */
    public static function ofObject(object $object): self
    {
        return new self($object::class);
    }

    /**
     * Retrieve the current instanceof.
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Checks if the instance could be nullable.
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }

    /**
     * Set the instance nullable.
     */
    public function nullable(): static
    {
        $this->nullable = true;

        return $this;
    }

    /**
     * Set the instance not nullable.
     */
    public function notNullable(): static
    {
        $this->nullable = false;

        return $this;
    }

    /**
     * Retrieve the constraint name.
     */
    public function getConstraintName(): string
    {
        return 'instanceof';
    }

    /**
     * Checks if a variable is allowed to be an instance of $class.
     */
    public function allows(mixed $instance): bool
    {
        return ($this->isNullable() && null === $instance)
            || (is_object($instance) && $instance instanceof $this->class);
    }
}
