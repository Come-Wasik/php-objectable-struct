<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Enums;

enum StructCasters: string
{
    case Integer = 'integer';
    case Float = 'float';
    case String = 'string';
    case Boolean = 'boolean';
    case Array = 'array';
    case Object = 'object';
    case Json = 'json';
    case Datetime = 'datetime';
    case Struct = 'struct';
}
