<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Constraints\Cast;

class ArrayCast extends Cast
{
    public function getTypeName(): string
    {
        return 'array';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [];
    }

    /**
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    public function canCast(mixed $value): bool
    {
        return true;
    }

    public function performCast(mixed $value): mixed
    {
        return (array) $value;
    }

    public function getDefaultValue(): mixed
    {
        return [];
    }
}
