<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Constraints\Cast;

class StringCast extends Cast
{
    public function getTypeName(): string
    {
        return 'string';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [];
    }

    public function canCast(mixed $value): bool
    {
        return is_scalar($value);
    }

    public function performCast(mixed $value): mixed
    {
        return (string) $value;
    }

    public function getDefaultValue(): mixed
    {
        return '';
    }
}
