<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Constraints\Cast;

class JsonCast extends Cast
{
    public function getTypeName(): string
    {
        return 'json';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [];
    }

    public function canCast(mixed $value): bool
    {
        return self::isJsonable($value);
    }

    public function performCast(mixed $value): mixed
    {
        return self::isJson($value) ? $value : json_encode($value);
    }

    public function getDefaultValue(): mixed
    {
        return '""';
    }

    /**
     * Checks if a value can be cast into a json.
     */
    public static function isJsonable(mixed $value): bool
    {
        // A valid json can be created only from an array or a scalar (int, float, string, bool)
        if ( ! is_array($value) && ! is_scalar($value)) {
            return false;
        }

        // Checks if the array or the string is jsonable
        json_encode($value);

        return JSON_ERROR_NONE === json_last_error();
    }

    /**
     * Checks if a value is a valid json.
     */
    public static function isJson(mixed $value): bool
    {
        if ( ! is_string($value)) {
            return false;
        }
        json_decode($value);

        return JSON_ERROR_NONE === json_last_error();
    }
}
