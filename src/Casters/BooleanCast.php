<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Constraints\Cast;

class BooleanCast extends Cast
{
    public function getTypeName(): string
    {
        return 'boolean';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [
            'bool',
        ];
    }

    public function canCast(mixed $value): bool
    {
        return null !== filter_var($value, FILTER_VALIDATE_BOOLEAN, [
            'flags' => FILTER_NULL_ON_FAILURE,
        ]);
    }

    public function performCast(mixed $value): mixed
    {
        return (bool) $value;
    }

    public function getDefaultValue(): mixed
    {
        return false;
    }
}
