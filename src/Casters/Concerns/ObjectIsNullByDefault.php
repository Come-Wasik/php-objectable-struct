<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters\Concerns;

trait ObjectIsNullByDefault
{
    public function getDefaultValue(): mixed
    {
        return null;
    }
}
