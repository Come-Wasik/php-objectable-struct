<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Casters\Concerns\ObjectIsNullByDefault;
use Nolikein\Objectable\Constraints\Cast;

class ObjectCast extends Cast
{
    use ObjectIsNullByDefault;

    public function getTypeName(): string
    {
        return 'object';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [];
    }

    public function canCast(mixed $value): bool
    {
        return ! is_object($value);
    }

    public function performCast(mixed $value): mixed
    {
        return null === $value
            ? $value
            : (object) $value;
    }
}
