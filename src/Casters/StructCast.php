<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Constraints\Cast;
use Nolikein\Objectable\Struct;

class StructCast extends Cast
{
    public function getTypeName(): string
    {
        return 'struct';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [];
    }

    public function canCast(mixed $value): bool
    {
        return is_array($value);
    }

    public function performCast(mixed $value): mixed
    {
        return new Struct($value);
    }

    public function getDefaultValue(): mixed
    {
        return new Struct();
    }
}
