<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Casters\Concerns\ObjectIsNullByDefault;
use Nolikein\Objectable\Constraints\Cast;

class DatetimeCast extends Cast
{
    use ObjectIsNullByDefault;

    public function getTypeName(): string
    {
        return 'datetime';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [];
    }

    public function canCast(mixed $value): bool
    {
        return null === $value || self::isDatetimable($value);
    }

    public function performCast(mixed $value): mixed
    {
        return null === $value ? null : ($value instanceof \DateTimeInterface ? $value : new \DateTime($value));
    }

    public function getDefaultValue(): mixed
    {
        return null;
    }

    /**
     * Checks if a value is datetimeable.
     */
    protected static function isDatetimable(mixed $datetimable): bool
    {
        if ($datetimable instanceof \DateTimeInterface) {
            return true;
        }
        if ( ! is_string($datetimable)) {
            return false;
        }
        $res = date_parse($datetimable);

        return 0 === $res['error_count'] && 0 === $res['warning_count'];
    }
}
