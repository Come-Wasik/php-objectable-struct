<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Casters;

use Nolikein\Objectable\Constraints\Cast;

class IntegerCast extends Cast
{
    public function getTypeName(): string
    {
        return 'integer';
    }

    /**
     * @return array<int, string>
     */
    public function getTypeAliases(): array
    {
        return [
            'int',
        ];
    }

    public function canCast(mixed $value): bool
    {
        return null !== filter_var($value, FILTER_VALIDATE_INT, [
            'flags' => FILTER_NULL_ON_FAILURE,
        ]);
    }

    public function performCast(mixed $value): mixed
    {
        return (int) $value;
    }

    public function getDefaultValue(): mixed
    {
        return 0;
    }
}
