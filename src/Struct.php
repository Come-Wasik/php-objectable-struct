<?php

declare(strict_types=1);

namespace Nolikein\Objectable;

use Nolikein\Objectable\Constraints\Cast;
use Nolikein\Objectable\Constraints\Instance;
use Nolikein\Objectable\Contracts\Arrayable;
use Nolikein\Objectable\Contracts\Jsonable;
use Nolikein\Objectable\Exceptions\AttributeDoesntExists;
use Nolikein\Objectable\Exceptions\CannotEncodeJson;
use Nolikein\Objectable\Exceptions\ConstraintClassNotSupported;
use Nolikein\Objectable\Exceptions\ConstraintNotDeclared;
use Nolikein\Objectable\Exceptions\MethodOnlyAcceptArraylist;
use Nolikein\Objectable\Exceptions\NotInstanceOf;
use Nolikein\Objectable\Exceptions\NotNullable;
use Nolikein\Objectable\Exceptions\SkipFilling;
use Nolikein\Objectable\Features\SupportsConstraint;
use Nolikein\Objectable\Features\SupportsHooks;
use Nolikein\Objectable\Features\SupportStrict;
use Nolikein\Objectable\Support\Arr;

/**
 * Objectable an array of data.
 *
 * Based on beautiful Laravel Models.
 *
 * @implements \ArrayAccess<string, mixed>
 */
class Struct implements Arrayable, \ArrayAccess, Jsonable, \JsonSerializable
{
    use SupportStrict;
    use SupportsConstraint;
    use SupportsHooks;

    /** @var array<string, mixed> The struct attributes */
    protected array $attributes = [];

    /**
     * Create a new Struct instance.
     *
     * @param array<string, mixed> $attributes
     * @param array<string, mixed> $constraints
     */
    public function __construct(
        array $attributes = [],
        array $constraints = [],
        bool $isStrict = false
    ) {
        // Checks if the strict mode must must be used
        $isStrict = $isStrict || $this->isStrict();
        // Disable it remporary, only during bootstrap to bypass any error
        $this->disableStrict();

        $this->useConstraintMethodIfNotEmpty();
        $this->ensureConstraintsAreInstances();

        $attributes = array_merge($this->attributes, $attributes);
        $constraints = array_merge($this->constraints, $constraints);

        if ($isStrict) {
            $this->ensureEachAttributeHasItsConstraint($attributes, $constraints);
            $this->fillBoth($attributes, $constraints);
            $this->enableStrict();
        } else {
            $this->fillBoth($attributes, $constraints);
        }
    }

    /**
     * Retrieve the model as a string.
     */
    public function __toString(): string
    {
        return $this->toJson();
    }

    /**
     * Set dynamically an attribute.
     */
    public function __set(string $name, mixed $value): void
    {
        $this->setAttribute($name, $value);
    }

    /**
     * Retrieve dynamically an attribute.
     */
    public function __get(string $name): mixed
    {
        return $this->getAttribute($name);
    }

    /**
     * Checks if an attribute is set.
     */
    public function __isset(string $name): bool
    {
        return $this->offsetExists($name);
    }

    /**
     * Unset an attribute.
     */
    public function __unset(string $name): void
    {
        $this->offsetUnset($name);
    }

    public function hasAttribute(string $name): bool
    {
        return key_exists($name, $this->attributes);
    }

    /**
     * Retrieve an attribute.
     */
    public function getAttribute(string $name): mixed
    {
        return $this->isStrict() && ! $this->hasAttribute($name)
            ? throw AttributeDoesntExists::make($this, $name) : ($this->attributes[$name] ?? null);
    }

    /**
     * Set an attribute in the attributes bag.
     */
    public function setAttribute(string $name, mixed $value): static
    {
        if ($this->isStrict() && ! $this->hasAttribute($name)) {
            throw AttributeDoesntExists::make($this, $name);
        }

        try {
            if ($this->usesHooks()) {
                // Action to perform before updating an attribute
                $this->updatingAttribute($name, $value);
            }

            $this->attributes[$name] = $value;
            $this->afterSetAttribute($name, $value);

            if ($this->usesHooks()) {
                // Action to perform after updated an attribute
                $this->updatedAttribute($name, $value);
            }
        } catch (SkipFilling $skip) {
            // This exception allows to skip an attribute to be filled
            return $this;
        }

        return $this;
    }

    /**
     * Set attributes from an array.
     *
     * @param array<string, mixed> $data
     */
    public function fill(array $data): static
    {
        if ([] !== $data && Arr::isList($data)) {
            throw MethodOnlyAcceptArraylist::make(__FUNCTION__);
        }

        foreach ($data as $name => $value) {
            $this->setAttribute($name, $value);
        }

        return $this;
    }

    /**
     * Retrieve the struct as JSON.
     *
     * @throws CannotEncodeJson
     */
    public function toJson(int $options = 0): string
    {
        $json = json_encode($this->toArray(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw CannotEncodeJson::make($this, json_last_error_msg());
        }

        return $json;
    }

    /**
     * Specify data which should be serialized to JSON. Serializes the object
     * to a value that can be serialized natively by json_encode().
     *
     * @return array<string, mixed> returns data which can be serialized by json_encode(),
     *                              which is a value of any type other than a resource
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    /**
     * Retrieve the struct as an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return $this->attributes;
    }

    /**
     * Whether or not an offset exists.
     */
    public function offsetExists(mixed $offset): bool
    {
        return $this->hasAttribute($offset);
    }

    /**
     * Offset to retrieve.
     * Returns the value at specified offset.
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->getAttribute($offset);
    }

    /**
     * Assigns a value to the specified offset.
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->setAttribute($offset, $value);
    }

    /**
     * Unsets an offset.
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->attributes[$offset]);
    }

    /**
     * Fill both attributes and constraints.
     *
     * @param array<string, mixed> $attributes
     * @param array<string, mixed> $constraints
     */
    public function fillBoth(array $attributes, array $constraints): void
    {
        $this->fill($attributes);
        $this->fillConstraints($constraints);
    }

    /**
     * Ensure constraints defined at class definition are transformed from string
     * or enum to Constraint instance.
     */
    public function ensureConstraintsAreInstances(): void
    {
        if ([] !== $this->constraints) {
            $this->fillConstraints($this->constraints);
        }
    }

    /**
     * Ensure that each given attribute has its constraint in the given bag.
     *
     * @param array<string, mixed> $attributes
     * @param array<string, mixed> $constraints
     */
    public function ensureEachAttributeHasItsConstraint(array $attributes, array $constraints): void
    {
        if ([] !== ($diffs = array_diff_key($attributes, $constraints))) {
            throw ConstraintNotDeclared::make(array_keys($diffs));
        }
    }

    /**
     * Initialize the constraints array either by method definition or by array content.
     * Method content always erase initial array content if not empty.
     */
    public function useConstraintMethodIfNotEmpty(): void
    {
        if ([] !== $this->constraints()) {
            $this->constraints = $this->constraints();
        }
    }

    /**
     * Perform an action before a struct attribute has been updated.
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    protected function updatingAttribute(string $name, mixed $value): void
    {
    }

    /**
     * Perform an action after a struct attribute has been fully updated.
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    protected function updatedAttribute(string $name, mixed $value): void
    {
    }

    /**
     * Perform actions after an attribute has been set.
     */
    protected function afterSetAttribute(string $name, mixed $value): void
    {
        // The strict mode force to use constraint
        if ($this->isStrict() && ! $this->hasConstraint($name)) {
            throw ConstraintNotDeclared::make([$name]);
        }
        if ($this->hasConstraint($name)) {
            $constraint = $this->getConstraint($name);

            if ($constraint instanceof Cast) {
                $this->castExistingAttribute($name, $constraint);
            } elseif ($constraint instanceof Instance) {
                if ( ! $constraint->allows($value)) {
                    if ( ! $constraint->isNullable()) {
                        throw NotNullable::make($name);
                    }
                    throw NotInstanceOf::make($name, $constraint);
                }
            } else {
                throw ConstraintClassNotSupported::make($constraint);
            }
        }
    }
}
