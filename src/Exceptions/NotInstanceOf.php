<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Constraints\Instance;
use Nolikein\Objectable\Exceptions\Abstracts\InstanceOfException;

final class NotInstanceOf extends InstanceOfException
{
    /**
     * Generate a new ConstraintException.
     */
    public static function make(string $name, Instance $constraint): self
    {
        return new self(sprintf(
            'The Struct should have the "%s" property with instance of "%s".',
            $name,
            $constraint->getClass()
        ));
    }
}
