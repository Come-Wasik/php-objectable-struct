<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

final class CannotEncodeJson extends \RuntimeException
{
    /**
     * Generate a new CannotEncodeJson from a Struct.
     */
    public static function make(object $object, string $errorMessage): self
    {
        return new self(sprintf(
            'The Struct "%s" coult not be encoded to JSON: %s',
            $object::class,
            $errorMessage
        ));
    }
}
