<?php

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\StructException;

class SkipFilling extends StructException
{
}
