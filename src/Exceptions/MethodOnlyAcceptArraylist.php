<?php

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\StructException;

class MethodOnlyAcceptArraylist extends StructException
{
    public static function make(string $method): self
    {
        return new self(
            sprintf(
                'The %s method only accept associative array.',
                $method
            )
        );
    }
}
