<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\ConstraintException;

final class ConstraintNotDeclared extends ConstraintException
{
    /**
     * @param array<int, string> $attributeNames
     */
    public static function make(array $attributeNames): self
    {
        return new self(sprintf(
            'The given attributes "%s" has no constraints but the strict mode was enabled.',
            implode(', ', $attributeNames)
        ));
    }
}
