<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Contracts\CastPattern;
use Nolikein\Objectable\Exceptions\Abstracts\CastException;

final class NotACastingType extends CastException
{
    public static function make(string $castName): self
    {
        return new self(sprintf(
            'The given class "%s" is not a casting type. Make sure your class inherit from the %s interface',
            $castName,
            CastPattern::class
        ));
    }
}
