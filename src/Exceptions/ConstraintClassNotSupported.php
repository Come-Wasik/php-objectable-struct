<?php

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Contracts\Constraint;
use Nolikein\Objectable\Exceptions\Abstracts\ConstraintException;

class ConstraintClassNotSupported extends ConstraintException
{
    public static function make(Constraint $constraint): self
    {
        return new self(sprintf(
            'The constraint name "%s" is not supported.',
            $constraint->getConstraintName()
        ));
    }
}
