<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\CastException;

final class CannotCast extends CastException
{
    public static function make(string $attributeName, string $castName): self
    {
        return new self(sprintf(
            'Your cannot cannot cast %s attribute to %s.',
            $attributeName,
            $castName,
        ));
    }
}
