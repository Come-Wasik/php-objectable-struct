<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\InstanceOfException;

final class NotNullable extends InstanceOfException
{
    /**
     * Generate a new ConstraintException.
     */
    public static function make(string $name): self
    {
        return new self(sprintf(
            'The property "%s" is not nullable.',
            $name,
        ));
    }
}
