<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\ConstraintException;

final class CannotChangeConstraint extends ConstraintException
{
    public static function make(string $attributeName): self
    {
        return new self(sprintf(
            'The given attribute "%s" cannot change its constraint since the strict mode is enabled.',
            $attributeName,
        ));
    }
}
