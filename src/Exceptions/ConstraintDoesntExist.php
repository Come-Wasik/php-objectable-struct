<?php

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\ConstraintException;

class ConstraintDoesntExist extends ConstraintException
{
    public static function make(string $classString): self
    {
        return new self(sprintf(
            'The class-string "%s" is not declared.',
            $classString
        ));
    }
}
