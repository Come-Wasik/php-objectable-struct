<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\ConstraintException;
use Nolikein\Objectable\Struct;

final class AttributeConstraintNotFound extends ConstraintException
{
    public static function make(Struct $struct, string $attributeName): self
    {
        return new self(sprintf(
            'The Struct "%s" hasn\'t defined a casting for the "%s" attribute. If you got this error, it\'s because you tried to retrieve the casts without checking it\'s existance.',
            $struct::class,
            $attributeName
        ));
    }
}
