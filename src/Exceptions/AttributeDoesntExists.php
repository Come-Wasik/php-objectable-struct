<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\StrictException;
use Nolikein\Objectable\Struct;

final class AttributeDoesntExists extends StrictException
{
    public static function make(Struct $struct, string $attributeName): self
    {
        return new self(sprintf(
            'The Struct "%s" does not contain the key "%s"',
            $struct::class,
            $attributeName
        ));
    }
}
