<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions;

use Nolikein\Objectable\Exceptions\Abstracts\ConstraintException;

final class UnexistingConstraint extends ConstraintException
{
    public static function make(string $constraint): self
    {
        return new self(sprintf(
            'The constraint name "%s" does not exists.',
            $constraint
        ));
    }
}
