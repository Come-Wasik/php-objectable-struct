<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Exceptions\Abstracts;

abstract class StructException extends \RuntimeException
{
}
