<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Features;

use Nolikein\Objectable\Casters\ArrayCast;
use Nolikein\Objectable\Casters\BooleanCast;
use Nolikein\Objectable\Casters\DatetimeCast;
use Nolikein\Objectable\Casters\FloatCast;
use Nolikein\Objectable\Casters\IntegerCast;
use Nolikein\Objectable\Casters\JsonCast;
use Nolikein\Objectable\Casters\ObjectCast;
use Nolikein\Objectable\Casters\StringCast;
use Nolikein\Objectable\Casters\StructCast;
use Nolikein\Objectable\Constraints\Cast;
use Nolikein\Objectable\Constraints\Instance;
use Nolikein\Objectable\Contracts\CastPattern;
use Nolikein\Objectable\Contracts\Constraint;
use Nolikein\Objectable\Exceptions\AttributeConstraintNotFound;
use Nolikein\Objectable\Exceptions\CannotCast;
use Nolikein\Objectable\Exceptions\CannotChangeConstraint;
use Nolikein\Objectable\Exceptions\ConstraintClassNotSupported;
use Nolikein\Objectable\Exceptions\ConstraintDoesntExist;
use Nolikein\Objectable\Exceptions\MethodOnlyAcceptArraylist;
use Nolikein\Objectable\Exceptions\NotACastingType;
use Nolikein\Objectable\Exceptions\NotInstanceOf;
use Nolikein\Objectable\Exceptions\NotNullable;
use Nolikein\Objectable\Exceptions\SkipFilling;
use Nolikein\Objectable\Exceptions\UnexistingConstraint;
use Nolikein\Objectable\Support\Arr;
use Nolikein\Objectable\Support\Str;

trait SupportsConstraint
{
    /** @var array<string, Constraint|string|\BackedEnum> The attributes schema to perform a casting. After initialization, contain exclusively Constraint instances */
    protected array $constraints = [];

    /** @var array<int, class-string> The available casting types */
    protected static array $availableCasters = [
        IntegerCast::class,
        FloatCast::class,
        StringCast::class,
        BooleanCast::class,
        ArrayCast::class,
        ObjectCast::class,
        JsonCast::class,
        DatetimeCast::class,
        StructCast::class,
    ];

    /**
     * Checks if a cast exists for an attribute.
     */
    public function hasConstraint(string $name): bool
    {
        return key_exists($name, $this->constraints);
    }

    /**
     * Retrieve a cast attached to an attribute.
     */
    public function getConstraint(string $name): Constraint
    {
        if ( ! $this->hasConstraint($name)) {
            throw AttributeConstraintNotFound::make($this, $name);
        }

        return $this->constraints[$name];
    }

    /**
     * Retrieve a new constraint instance from a string name.
     */
    public function newConstraintFromString(string $constraint): Constraint
    {
        if (Str::startsWith('instanceof:', $constraint)) {
            return Instance::of(Str::after('instanceof:', $constraint));
        }

        if ($this->hasCasterFromClassString($constraint)) {
            return new $constraint();
        }

        foreach (self::$availableCasters as $classString) {
            /** @var Cast $caster */
            $caster = new $classString();
            if ($this->matchCasterName($constraint, $caster)) {
                return $caster;
            }
            unset($caster);
        }
        throw UnexistingConstraint::make($constraint);
    }

    /**
     * Set a new constraints for an attribute and auto set an attribute.
     */
    public function setConstraint(string $name, Constraint|string|\BackedEnum $constraint): static
    {
        if ($this->isStrict()) {
            throw CannotChangeConstraint::make($name);
        }

        // Convert any constraint format to an instance of Constraint
        if (is_string($constraint)) {
            $constraint = $this->newConstraintFromString($constraint);
        } elseif ($constraint instanceof \BackedEnum) {
            $constraint = $this->newConstraintFromString($constraint->value);
        }

        try {
            if ($this->usesHooks()) {
                // Action to perform before updating a constraint
                $this->updatingConstraint($name, $constraint);
            }

            $this->constraints[$name] = $constraint;
            $this->afterSetConstraint($name, $constraint);

            if ($this->usesHooks()) {
                // Action to perform after updated a constraint
                $this->updatedConstraint($name, $constraint);
            }
        } catch (SkipFilling $skip) {
            // This exception allows to skip an constraint to be filled
            return $this;
        }

        return $this;
    }

    /**
     * Set constraints from an array.
     *
     * @param array<string, Constraint> $constraints
     */
    public function fillConstraints(array $constraints): static
    {
        if ([] !== $constraints && Arr::isList($constraints)) {
            throw MethodOnlyAcceptArraylist::make(__FUNCTION__);
        }
        foreach ($constraints as $name => $constraint) {
            $this->setConstraint($name, $constraint);
        }

        return $this;
    }

    /**
     * Remove a defined constraint.
     */
    public function removeConstraint(string $name): void
    {
        if ($this->isStrict()) {
            throw CannotChangeConstraint::make($name);
        }
        if ($this->hasConstraint($name)) {
            unset($this->constraints[$name]);
        }
    }

    /**
     * Checks if the Struct class supports a caster from instance.
     */
    public static function hasCasterFromInstance(CastPattern $caster): bool
    {
        return in_array($caster::class, self::$availableCasters);
    }

    /**
     * Checks if the Struct class supports a caster from class-string.
     */
    public static function hasCasterFromClassString(string $classString): bool
    {
        return in_array($classString, self::$availableCasters);
    }

    /**
     * Checks if the Struct class supports a caster from name.
     */
    public static function hasCasterFromName(string $name): bool
    {
        $self = new self();
        foreach (self::$availableCasters as $castingType) {
            if ($self->matchCasterName($name, new $castingType())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if a struct class supports a given caster.
     */
    public static function hasCaster(string|CastPattern $caster): bool
    {
        if ($caster instanceof CastPattern) {
            return self::hasCasterFromInstance($caster);
        }

        return self::hasCasterFromClassString($caster)
            || self::hasCasterFromName($caster);
    }

    /**
     * Register a new caster from instance.
     */
    public static function addCasterFromInstance(CastPattern $caster): void
    {
        self::$availableCasters[] = $caster::class;
    }

    /**
     * Register a new caster from class-string.
     *
     * @param class-string $classString
     */
    public static function addCasterFromClassString(string $classString): void
    {
        if ( ! class_exists($classString)) {
            throw ConstraintDoesntExist::make($classString);
        }
        $instance = new $classString();
        if ( ! ($instance instanceof CastPattern)) {
            throw NotACastingType::make($classString);
        }
        self::$availableCasters[] = $classString;
    }

    /**
     * Register a new casting type.
     */
    public static function addCaster(string|CastPattern $caster): void
    {
        if (is_string($caster)) {
            self::addCasterFromClassString($caster);
        } else {
            self::addCasterFromInstance($caster);
        }
    }

    /**
     * Set the constraints from function. Usefull to define instances.
     *
     * @return array<string, string|Constraint>
     */
    protected function constraints(): array
    {
        return [];
    }

    /**
     * Actions to do after a constraint has been set.
     */
    protected function afterSetConstraint(string $name, Constraint $constraint): void
    {
        if ($constraint instanceof Cast) {
            $this->castAttributeEvenIfNotExists($name, $constraint);
        } elseif ($constraint instanceof Instance) {
            if ($this->hasAttribute($name)) {
                $value = $this->getAttribute($name);

                if ( ! $constraint->allows($value)) {
                    if ( ! $constraint->isNullable()) {
                        throw NotNullable::make($name);
                    }
                    throw NotInstanceOf::make($name, $constraint);
                }
            } else {
                // By default, all instances are null
                $this->attributes[$name] = null;
            }
        } else {
            throw ConstraintClassNotSupported::make($constraint);
        }
    }

    /**
     * Perform cast on an existing attribute.
     */
    protected function castExistingAttribute(string $name, Cast $caster): void
    {
        $value = $this->getAttribute($name);

        if ( ! $caster->allows($value)) {
            throw CannotCast::make($name, $caster->getTypeName());
        }
        $this->attributes[$name] = $caster->performCast($value);
    }

    /**
     * Perform cast on an attribute even if it does not exists.
     */
    protected function castAttributeEvenIfNotExists(string $name, Cast $caster): void
    {
        if ($this->hasAttribute($name)) {
            $value = $this->getAttribute($name);

            if ( ! $caster->allows($value)) {
                throw CannotCast::make($name, $caster->getTypeName());
            }
            $this->attributes[$name] = $caster->performCast($value);
        } else {
            $this->attributes[$name] = $caster->getDefaultValue();
        }
    }

    /**
     * Checks whether the given type match with a CastPattern instance.
     */
    protected function matchCasterTrueName(string $given, Cast $castingType): bool
    {
        return $given === $castingType->getTypeName();
    }

    /**
     * Checks whether the given type is an alias of a CastPattern instance.
     */
    protected function matchCasterAlias(string $given, Cast $castingType): bool
    {
        return in_array($given, $castingType->getTypeAliases());
    }

    /**
     * Checks whether the given type match a CastPattern instance.
     */
    protected function matchCasterName(string $given, Cast $castingType): bool
    {
        return $this->matchCasterTrueName($given, $castingType)
            || $this->matchCasterAlias($given, $castingType);
    }

    /**
     * Perform an action before a struct constraint has been updated.
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    protected function updatingConstraint(string $name, Constraint $value): void
    {
    }

    /**
     * Perform an action after a struct constraint has been fully updated.
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    protected function updatedConstraint(string $name, Constraint $value): void
    {
    }
}
