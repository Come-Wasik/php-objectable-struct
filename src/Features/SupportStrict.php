<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Features;

trait SupportStrict
{
    /** @var bool Enable the strict mode. Throw an exception when key doesnt exists. */
    protected bool $strictMode = false;

    /**
     * Create a new Struct but strict instance.
     *
     * @param array<string, mixed> $attributes
     * @param array<string, mixed> $constraints
     */
    public static function strict(array $constraints = [], array $attributes = []): self
    {
        return new self(
            attributes: $attributes,
            constraints: $constraints,
            isStrict: true
        );
    }

    /**
     * Tells if the struct has strict mode enables.
     */
    public function isStrict(): bool
    {
        return $this->strictMode;
    }

    /**
     * Enable the strict mode.
     */
    public function enableStrict(): static
    {
        $this->strictMode = true;

        return $this;
    }

    /**
     * Disable the strict mode.
     */
    public function disableStrict(): static
    {
        $this->strictMode = false;

        return $this;
    }
}
