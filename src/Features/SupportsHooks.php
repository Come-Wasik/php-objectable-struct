<?php

namespace Nolikein\Objectable\Features;

use Nolikein\Objectable\Exceptions\SkipFilling;

trait SupportsHooks
{
    /** @var bool Tells if we should trigger hooks */
    protected bool $triggerHooks = true;

    /**
     * Checks if the hooks feature is enabled.
     */
    public function usesHooks(): bool
    {
        return $this->triggerHooks;
    }

    /**
     * Disable the hooks.
     */
    public function disableHooks(): self
    {
        $this->triggerHooks = false;

        return $this;
    }

    /**
     * Enable the hooks.
     */
    public function enableHooks(): self
    {
        $this->triggerHooks = true;

        return $this;
    }

    /**
     * Execute an action without trigerring any hook.
     */
    public function withoutHooks(callable $fn): self
    {
        $this->disableHooks();
        $fn($this);
        $this->enableHooks();

        return $this;
    }

    /**
     * Skip an attribute filling.
     */
    protected function skipFilling(): never
    {
        throw new SkipFilling('You are not supposed to update this property.');
    }
}
