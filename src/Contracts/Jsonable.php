<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Contracts;

interface Jsonable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @source https://laravel.com/api/9.x/Illuminate/Contracts/Support/Jsonable.html
     */
    public function toJson(int $options = 0): string;
}
