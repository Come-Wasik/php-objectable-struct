<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Contracts;

interface Arrayable
{
    /**
     * Get the instance as an array.
     *
     * @return array<string, mixed>
     *
     * @source https://laravel.com/api/9.x/Illuminate/Contracts/Support/Arrayable.html
     */
    public function toArray(): array;
}
