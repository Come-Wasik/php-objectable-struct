<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Contracts;

interface CastPattern
{
    /**
     * Retrieve the type name.
     */
    public function getTypeName(): string;

    /**
     * Retrieve the type name aliases.
     *
     * @return array<int, string>
     */
    public function getTypeAliases(): array;

    /**
     * Checks if can cast to the current type.
     */
    public function canCast(mixed $value): bool;

    /**
     * Perform the cast from a value to the current type.
     */
    public function performCast(mixed $value): mixed;

    /**
     * Retrieve the type default value.
     */
    public function getDefaultValue(): mixed;
}
