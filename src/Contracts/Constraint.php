<?php

declare(strict_types=1);

namespace Nolikein\Objectable\Contracts;

interface Constraint
{
    /**
     * Retrieve the constraint name.
     */
    public function getConstraintName(): string;

    /**
     * Checks if a constraint is valid.
     */
    public function allows(mixed $mixed): bool;
}
