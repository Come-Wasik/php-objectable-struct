<?php

use Nolikein\Objectable\Casters\BooleanCast;
use Nolikein\Objectable\Contracts\Constraint;
use Nolikein\Objectable\Struct;

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

uses(Tests\TestCase::class)->in('Feature');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

expect()->extend('toBeOne', function () {
    return $this->toBe(1);
});

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

function something()
{
    // ..
}

function newFakeClass()
{
    return new class() {
    };
}

function newHookableStruct(array $attributes = [], array $constraints = [], bool $isStrict = false): Struct
{
    return new class($attributes, $constraints, $isStrict) extends Struct {
        /** @var array<string, mixed> The struct attributes */
        protected array $attributes = [
            'updatingAttribute' => false,
            'updatedAttribute' => false,
            'updatingConstraint' => false,
            'updatedConstraint' => false,
        ];

        protected function updatingAttribute(string $name, mixed $value): void
        {
            if ('updatingAttribute' === $name && true === $value) {
                throw new \RuntimeException('updatingAttribute');
            }
        }

        protected function updatedAttribute(string $name, mixed $value): void
        {
            if ('updatedAttribute' === $name && true === $value) {
                throw new \RuntimeException('updatedAttribute');
            }
        }

        protected function updatingConstraint(string $name, Constraint $constraint): void
        {
            if ('updatingConstraint' === $name && $constraint instanceof BooleanCast) {
                throw new \RuntimeException('updatingConstraint');
            }
        }

        protected function updatedConstraint(string $name, Constraint $constraint): void
        {
            if ('updatedConstraint' === $name && $constraint instanceof BooleanCast) {
                throw new \RuntimeException('updatedConstraint');
            }
        }
    };
}

function newPricingResultStruct(array $attributes = [], array $constraints = [], bool $isStrict = false): Struct
{
    return new class($attributes, $constraints, $isStrict) extends Struct {
        /** @var array<string, Constraint|string|\BackedEnum> The attributes schema to perform a casting. After initialization, contain exclusively Constraint instances */
        protected array $constraints = [
            'price' => 'float',
            'quantity' => 'float',
            'total' => 'float',
        ];

        /**
         * The data will be updated.
         */
        protected function updatingAttribute(string $name, mixed $value): void
        {
            // Avoid any developper to update the total manually
            if ('total' === $name) {
                $this->skipFilling();
            }
        }

        /**
         * The data has already been updated.
         */
        protected function updatedAttribute(string $name, mixed $value): void
        {
            // If the attribute that has been updated is named "price" or "quantity"
            // Calculate the total from the price and quantity

            // Use manual hook disabling
            if ('price' === $name) {
                $this->disableHooks();
                $this->total = $this->price * $this->quantity;
                $this->enableHooks();
            }

            // Use decorator to disable hooks
            if ('quantity' === $name) {
                $this->withoutHooks(fn () => $this->total = $this->price * $this->quantity);
            }
        }
    };
}
