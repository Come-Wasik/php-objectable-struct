<?php

use Nolikein\Objectable\Exceptions\AttributeDoesntExists;
use Nolikein\Objectable\Exceptions\CannotChangeConstraint;
use Nolikein\Objectable\Exceptions\ConstraintNotDeclared;
use Nolikein\Objectable\Struct;

it('can create a strict structure', function (): void {
    $s = Struct::strict([
        'myBoolean' => 'bool',
        'myString' => 'string',
    ]);

    expect($s->isStrict())->toBeTrue();
});

test('a normal struct has the strict mode disabled', function (): void {
    $s = new Struct();
    expect($s->isStrict())->toBeFalse();
});

it('can retrieve or set a key defined', function (): void {
    $s = Struct::strict([
        'myBoolean' => 'bool',
        'myString' => 'string',
    ]);

    expect($s->myBoolean)->toBe(false);
    expect($s->myString)->toBe('');

    $s->myBoolean = true;
    $s->myString = 'hello';
});

it('cannot retrieve or set a key not defined', function (): void {
    $s = Struct::strict([
        'myBoolean' => 'bool',
        'myString' => 'string',
    ]);

    expect(fn () => $s->doesntExists)
        ->toThrow(AttributeDoesntExists::class);
    expect(fn () => $s->doesntExists = 'value')
        ->toThrow(AttributeDoesntExists::class);
});

test('can set values from constructor', function (): void {
    $s = Struct::strict([
        'myBoolean' => 'bool',
        'myString' => 'string',
    ], [
        'myBoolean' => true,
        'myString' => 'world',
    ]);

    expect($s)
        ->myBoolean->tobeTrue()
        ->myString->toBe('world')
    ;
});

it('can set only constraint to have default values but only from constructor', function () {
    $s = Struct::strict([
        'myBoolean' => 'bool',
        'myString' => 'string',
    ]);

    expect($s)
        ->myBoolean->toBeBool()
        ->myString->toBeString()
    ;
});

it('should define constraint for all defined attributes', function () {
    expect(function (): void {
        Struct::strict([], ['undeclared' => 'value']);
    })->toThrow(ConstraintNotDeclared::class);
});

it('cannot change constraint after constructor', function () {
    $s = Struct::strict();

    expect(fn () => $s->setConstraint('doesnt-work', 'string'))
        ->toThrow(CannotChangeConstraint::class);

    expect(fn () => $s->fillConstraints(
        ['doesnt-work' => 'string']))
            ->toThrow(CannotChangeConstraint::class);

    expect(fn () => $s->removeConstraint('doesnt-work'))
        ->toThrow(CannotChangeConstraint::class);
});

test('can be defined from class', function (): void {
    $s = new class() extends Struct {
        protected array $constraints = [
            'myString' => 'string',
            'myInt' => 'integer',
            'myfloat' => 'float',
            'myBoolean' => 'boolean',
            'myJson' => 'json',
        ];

        protected bool $strictMode = true;
    };

    expect($s)
            ->myString->toBe('')
            ->myInt->toBe(0)
            ->myfloat->toBe(0.0)
            ->myBoolean->toBeFalse()
            ->myJson->toBe('""')
    ;

    expect($s->isStrict())->toBeTrue();
});
