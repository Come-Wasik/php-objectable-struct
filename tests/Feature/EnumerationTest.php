<?php

use Nolikein\Objectable\Enums\StructCasters;
use Nolikein\Objectable\Struct;

enum CustomTypeEnum: string
{
    case MyInt = 'int';
    case MyString = 'string';
}

it('can cast from enums', function (): void {
    $s = new Struct([
        'myInt' => '1',
        'myString' => 1,
    ], [
        'myInt' => StructCasters::Integer,
        'myString' => StructCasters::String,
    ]);

    expect($s)
        ->myInt->toBeInt()
        ->myString->toBeString()
    ;
});

it('can cast from custom enum', function (): void {
    $s = new Struct([
        'myInt' => '1',
        'myString' => 1,
    ], [
        'myInt' => \CustomTypeEnum::MyInt,
        'myString' => \CustomTypeEnum::MyString,
    ]);

    expect($s)
        ->myInt->toBeInt()
        ->myString->toBeString()
    ;
});
