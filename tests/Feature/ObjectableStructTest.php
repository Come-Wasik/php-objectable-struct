<?php

use Nolikein\Objectable\Exceptions\AttributeConstraintNotFound;
use Nolikein\Objectable\Struct;
use Nolikein\Objectable\Casters\ArrayCast;

it('assert a struct is a dark container', function (): void {
    $s = new Struct();
    expect($s->attributes)->toBeNull();
});

it('can set and get attributes', function (): void {
    $s = new Struct();

    // Setter are chainables
    $s->setAttribute('hello', 'world')
        ->setAttribute('world', 'hello');

    // Getter
    expect($s->getAttribute('hello'))->toBe('world');
    expect($s->getAttribute('world'))->toBe('hello');
    expect($s->getAttribute('inexistant'))->toBeNull();
});

it('assert a struct can be filled with anything at start even after', function (): void {
    // Initial state
    $s = new Struct();
    expect($s->getAttribute('hello'))->toBeNull();

    // Fill by constructor
    $s = new Struct([
        'sentence' => 'world',
        'number' => 123,
        'boolean' => true,
        'float' => 17.5,
    ]);
    expect($s)
        ->getAttribute('sentence')->toBe('world')
        ->getAttribute('number')->toBe(123)
        ->getAttribute('boolean')->toBe(true)
        ->getAttribute('float')->toBe(17.5)
    ;

    // Fill by method change or set new values
    $s->fill([
        'sentence' => 'entity',
        'newvalue' => 'because',
    ]);
    expect($s)
        ->getAttribute('sentence')->toBe('entity')
        ->getAttribute('newvalue')->toBe('because')
    ;
});

it('assert a struct is arrayable', function (): void {
    $s = new Struct();
    expect($s->toArray())->toBeArray();
});

it('assert a struct is jsonable', function (): void {
    $s = new Struct();
    expect($s->toJson())->toBeJson();
    expect($s->jsonSerialize())->toBeArray();
});

it('assert a struct can be managed like an array', function (): void {
    $s = new Struct([
        'hello' => 'world',
    ]);
    // Inexistant data is null by default
    expect($s['hello'])->toBe('world');

    // Setter
    $s['hello'] = 'not world';
    // Getter
    expect($s['hello'])->toBe('not world');

    // Offset exists
    expect(isset($s['hello']))->toBeTrue();
    expect(isset($s['world']))->toBeFalse();
    expect($s->offsetExists('hello'))->toBeTrue();
    expect($s->offsetExists('world'))->toBeFalse();

    // Unset is possible
    unset($s['hello']);
    expect($s['hello'])->toBeNull();
});

it('assert a struct can be managed like an object with properties', function (): void {
    $s = new Struct([
        'hello' => 'world',
    ]);
    // Inexistant data is null by default
    expect($s->hello)->tobe('world');

    // Setter
    $s->hello = 'not world';
    // Getter
    expect($s->hello)->toBe('not world');

    // Offset exists
    expect(isset($s->hello))->toBeTrue();
    expect(isset($s->world))->toBeFalse();
    expect($s->offsetExists('hello'))->toBeTrue();
    expect($s->offsetExists('world'))->toBeFalse();

    // Unset is possible
    unset($s->hello);
    expect($s->hello)->toBeNull();
});
