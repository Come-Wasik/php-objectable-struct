<?php

use Nolikein\Objectable\Constraints\Instance;
use Nolikein\Objectable\Exceptions\NotInstanceOf;
use Nolikein\Objectable\Exceptions\NotNullable;
use Nolikein\Objectable\Struct;

it('supports constraints', function () {
    $myClass = newFakeClass();

    enum MyBackedEnum: string
    {
        case Example = 'example';
    }

    new Struct([
        'myClass' => $myClass,
        'myEnum' => MyBackedEnum::Example,
        'myEnumCheck2' => MyBackedEnum::Example,
    ], [
        // Instance works with objects
        'myClass' => Instance::of($myClass::class),
        // Instance works with enumerations
        'myEnum' => Instance::of(MyBackedEnum::class),
        // Instance can declare instances
        'myEnumCheck2' => Instance::ofObject(MyBackedEnum::Example),
    ]);

    // The test doesnt expect anything special
    expect(true)->toBeTrue();
});

it('should use object of specific type with instance::of', function (): void {
    $myClass = newFakeClass();

    $s = new Struct([
        'myValue' => 'class',
    ]);

    expect(fn () => $s->setConstraint('myValue', Instance::of($myClass::class)))
        ->toThrow(NotInstanceOf::class);

    $s->myValue = $myClass;
    expect(fn () => $s->setConstraint('myValue', Instance::of(MyBackedEnum::class)))
            ->toThrow(NotInstanceOf::class);
});

test('instance:of is nullable by default and can toggle the nullable mode', function (): void {
    $instanceof = Instance::of('test');
    expect($instanceof->isNullable())->toBeTrue();

    $instanceof->notNullable();
    expect($instanceof->isNullable())->toBeFalse();

    $instanceof->nullable();
    expect($instanceof->isNullable())->toBeTrue();
});

it('accepts instances to be not nullable', function (): void {
    $myClass = newFakeClass();

    $s = new Struct([
        'test' => $myClass,
    ], [
        'test' => Instance::of($myClass::class)->notNullable(),
    ]);

    $s->setConstraint('test', Instance::of($myClass::class)->notNullable());
    expect(fn () => $s->test = null)->toThrow(NotNullable::class);
});

it('cannot take null argument if not null instance has been set into constructor', function (): void {
    $myClass = newFakeClass();

    new Struct([
        'test' => null,
    ], [
        'test' => Instance::of($myClass::class)->notNullable(),
    ]);
})->throws(NotNullable::class);
