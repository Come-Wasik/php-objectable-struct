<?php

use Nolikein\Objectable\Casters\ArrayCast;
use Nolikein\Objectable\Casters\BooleanCast;
use Nolikein\Objectable\Casters\FloatCast;
use Nolikein\Objectable\Casters\IntegerCast;
use Nolikein\Objectable\Casters\StringCast;
use Nolikein\Objectable\Constraints\Cast;
use Nolikein\Objectable\Exceptions\AttributeConstraintNotFound;
use Nolikein\Objectable\Exceptions\MethodOnlyAcceptArraylist;
use Nolikein\Objectable\Exceptions\NotACastingType;
use Nolikein\Objectable\Exceptions\UnexistingConstraint;
use Nolikein\Objectable\Struct;

/**
 * Feature: Cast.
 */
it('can apply a string cast from multiple way', function (): void {
    $s = new Struct(['myString' => 123]);
    expect($s->myString)->toBeInt()->toBe(123);

    // Instance form
    $s->setConstraint('myString', new StringCast());
    expect($s->myString)->toBeString()->toBe('123');

    $s->removeConstraint('myString');
    $s->myString = 123;
    expect($s->myString)->toBeInt()->toBe(123);

    // Class-string form
    $s->setConstraint('myString', StringCast::class);
    expect($s->myString)->toBeString()->toBe('123');

    $s->removeConstraint('myString');
    $s->myString = 123;
    expect($s->myString)->toBeInt()->toBe(123);

    // String name form
    $s->setConstraint('myString', 'string');
    expect($s->myString)->toBeString()->toBe('123');
});

it('can cast json', function (): void {
    $s = new Struct();

    $s->setConstraint('myJson', 'json');

    $s->myJson = 'hello';
    expect($s->myJson)->toBeJson()->toBe('"hello"');
});

it('can cast datetime', function (): void {
    $s = new Struct();
    $s->setConstraint('myDatetime', 'datetime');

    // Cast a date as string
    $s->myDatetime = '2023-02-02T16:07:27+01:00';
    expect($s->myDatetime)->toBeInstanceOf(\DateTimeInterface::class);
    $s->myDatetime = 'now';
    expect($s->myDatetime)->toBeInstanceOf(\DateTimeInterface::class);

    // Supports native Datetime instances
    $s->myDatetime = new \DateTime();
    expect($s->myDatetime)->toBeInstanceOf(\DateTimeInterface::class);
});

it('can apply casts from the constructor with multiple way', function (): void {
    $s = new Struct([
        'myString' => 123,
        'myInt' => '123',
        'myBool' => 1,
    ], [
        'myString' => new StringCast(),
        'myInt' => IntegerCast::class,
        'myBool' => 'boolean',
    ]);

    expect($s->myString)->toBeString()->toBe('123');
    expect($s->myInt)->toBeInt()->toBe(123);
    expect($s->myBool)->toBeBool()->toBe(true);
});

it('gives a default value if the attribute doesnt exists', function () {
    $s = new Struct();
    $s->setConstraint('unknown', 'string');
    expect($s->unknown)->toBeString()->toBe('');
});

it('can fill many constraints at the same time', function (): void {
    $s = new Struct();
    $s->fillConstraints([
        'myString' => 'string',
        'myInt' => 'int',
    ]);

    expect($s->myString)->toBeString()->toBe('');
    expect($s->myInt)->toBeInt()->toBe(0);
});

test('filling constraints only accept associative array', function () {
    $s = new Struct();
    expect(fn () => $s->fillConstraints(['myString', 'myInt']))
        ->toThrow(MethodOnlyAcceptArraylist::class, 'The fillConstraints method only accept associative array.');
});

test('caster are valid', function (): void {
    $s = new Struct([
        'myString' => 'string',
        'myInt' => '123',
        'myfloat' => '1.0',
        'myBoolean' => 'true',
        'myArray' => 'array',
        'myObject' => 'object',
        'myJson' => 'json',
        'myDatetime' => 'now',
        'myStruct' => ['key' => 'value'],
    ], [
        'myString' => 'string',
        'myInt' => 'integer',
        'myfloat' => 'float',
        'myBoolean' => 'boolean',
        'myArray' => 'array',
        'myObject' => 'object',
        'myJson' => 'json',
        'myDatetime' => 'datetime',
        'myStruct' => 'struct',
    ]);

    expect($s)
            ->myString->toBeString()
            ->myInt->toBeInt()
            ->myfloat->toBeFloat()
            ->myArray->toBeArray()
            ->myObject->toBeObject()
            ->myBoolean->toBeBool()
            ->myJson->toBeString()->toBe('"json"')
            ->myDatetime->toBeInstanceOf(DateTime::class)
            ->myStruct->toBeInstanceOf(Struct::class)
    ;
});

test('default value for casters is valid', function (): void {
    $s = new Struct();

    $s->fillConstraints([
        'myString' => 'string',
        'myInt' => 'integer',
        'myfloat' => 'float',
        'myBoolean' => 'boolean',
        'myArray' => 'array',
        'myObject' => 'object',
        'myJson' => 'json',
        'myDatetime' => 'datetime',
        'myStruct' => 'struct',
    ]);

    expect($s)
        ->myString->toBe('')
        ->myInt->toBe(0)
        ->myfloat->toBe(0.0)
        ->myArray->toBe([])
        ->myObject->toBe(null)
        ->myBoolean->toBeFalse()
        ->myJson->toBe('""')
        ->myDatetime->toBeNull()
        ->myStruct->toBeInstanceOf(Struct::class)
    ;
});

it('cannot cast to unexisting caster', function (): void {
    $s = new Struct([], [
        'myValue' => 'doesnt-exists',
    ]);
})->throws(UnexistingConstraint::class);

it('cannot retrieve cast if doesnt declared', function (): void {
    $s = new Struct();

    $s->getConstraint('myString');
})->throws(AttributeConstraintNotFound::class);

it('can checks if struct supports caster from name', function (): void {
    expect(Struct::hasCaster('string'))->toBeTrue();
    expect(Struct::hasCaster('blubibulga'))->toBeFalse();
    expect(Struct::hasCasterFromName('string'))->toBeTrue();
    expect(Struct::hasCasterFromClassString('string'))->toBeFalse();
});

it('can checks if struct supports caster from class string', function (): void {
    expect(Struct::hasCaster(StringCast::class))->toBeTrue();
    expect(Struct::hasCasterFromName(StringCast::class))->toBeFalse();
    expect(Struct::hasCasterFromClassString(StringCast::class))->toBeTrue();
});

it('can checks if struct supports caster from instance', function (): void {
    expect(Struct::hasCaster(new StringCast()))->toBeTrue();
    expect(Struct::hasCasterFromInstance(new StringCast()))->toBeTrue();

    // Create a new cast that will not be implemented
    $unexisting = new class() extends Cast {
        public function getTypeName(): string
        {
            return 'struct';
        }

        /**
         * @return array<int, string>
         */
        public function getTypeAliases(): array
        {
            return [];
        }

        public function canCast(mixed $value): bool
        {
            return is_array($value);
        }

        public function performCast(mixed $value): mixed
        {
            return new Struct($value);
        }

        public function getDefaultValue(): mixed
        {
            return new Struct();
        }
    };
    expect(Struct::hasCasterFromInstance($unexisting))->toBeFalse();
});

/**
 * Feature: Schema by inheritance.
 */
it('can casting from array schema', function (): void {
    $s = new class() extends Struct {
        // Array way force to use constants-like elements
        protected array $constraints = [
            'myString' => 'string',
            'myInt' => 'integer',
            'myfloat' => 'float',
            'myBoolean' => 'boolean',
            'myJson' => 'json',
            'myDatetime' => 'datetime',
        ];
    };

    expect($s)
        ->myString->toBe('')
        ->myInt->toBe(0)
        ->myfloat->toBe(0.0)
        ->myBoolean->toBeFalse()
        ->myJson->toBe('""')
        ->myDatetime->toBeNull()
    ;

    $s->myString = 123;
    $s->myInt = '123';
    $s->myfloat = '123.45';
    $s->myBoolean = 'true';
    $s->myJson = ['hello' => 'world'];
    $s->myDatetime = new \DateTime();

    expect($s->myString)->toBeString()->toBe('123');
    expect($s->myInt)->toBeInt()->tobe(123);
    expect($s->myfloat)->toBeFloat()->toBe(123.45);
    expect($s->myBoolean)->toBeBool()->toBe(true);
    expect($s->myJson)->toBeJson()->tobe('{"hello":"world"}');
    expect($s->myDatetime)->toBeInstanceOf(\DateTimeInterface::class);
});

it('can casting from method schema', function (): void {
    $s = new class() extends Struct {
        // Array way allows to use instances
        protected function constraints(): array
        {
            return [
                'myString' => StringCast::class,
                'myInt' => IntegerCast::class,
                'myfloat' => new FloatCast(),
                'myBoolean' => new BooleanCast(),
                'myJson' => 'json',
                'myDatetime' => 'datetime',
            ];
        }
    };

    expect($s)
        ->myString->toBe('')
        ->myInt->toBe(0)
        ->myfloat->toBe(0.0)
        ->myBoolean->toBeFalse()
        ->myJson->toBe('""')
        ->myDatetime->toBeNull()
    ;

    $s->myString = 123;
    $s->myInt = '123';
    $s->myfloat = '123.45';
    $s->myBoolean = 'true';
    $s->myJson = ['hello' => 'world'];
    $s->myDatetime = new \DateTime();

    expect($s->myString)->toBeString()->toBe('123');
    expect($s->myInt)->toBeInt()->tobe(123);
    expect($s->myfloat)->toBeFloat()->toBe(123.45);
    expect($s->myBoolean)->toBeBool()->toBe(true);
    expect($s->myJson)->toBeJson()->tobe('{"hello":"world"}');
    expect($s->myDatetime)->toBeInstanceOf(\DateTimeInterface::class);
});

/**
 * Feature: Add custom CastPattern classes.
 */
it('can register new cast', function (): void {
    $newArray = new class() extends ArrayCast {
        public function getTypeName(): string
        {
            return 'new-array-1';
        }
    };
    expect(Struct::hasCaster($newArray->getTypeName()))->toBeFalse();
    Struct::addCaster($newArray::class);
    expect(Struct::hasCaster($newArray->getTypeName()))->toBeTrue();
});

it('can register new cast by using addCasterFromClassString', function (): void {
    $newArray = new class() extends ArrayCast {
        public function getTypeName(): string
        {
            return 'new-array-2';
        }
    };

    expect(Struct::hasCaster($newArray->getTypeName()))->toBeFalse();
    Struct::addCasterFromClassString($newArray::class);
    expect(Struct::hasCaster($newArray->getTypeName()))->toBeTrue();
});

it('can register new cast by using addCasterFromInstance', function () {
    $newArray = new class() extends ArrayCast {
        public function getTypeName(): string
        {
            return 'new-array-3';
        }
    };

    expect(Struct::hasCaster($newArray->getTypeName()))->toBeFalse();
    Struct::addCasterFromInstance($newArray);
    expect(Struct::hasCaster($newArray->getTypeName()))->toBeTrue();
});

it('cannot register cast that not implements the TypeCasting interface', function (): void {
    $notACastingType = new class() {
        // Nothing to do
    };
    Struct::addCaster($notACastingType::class);
})->throws(NotACastingType::class);

it('can cast to a struct if give an array', function () {
    $s = new Struct([
        'myStruct' => [
            'a' => 1,
            'b' => 2,
            'c' => 3,
        ],
    ], [
        'myStruct' => 'struct',
    ]);

    expect($s)
        ->myStruct->toBeInstanceOf(Struct::class)
        ->myStruct->a->toBe(1)
        ->myStruct->b->toBe(2)
        ->myStruct->c->toBe(3)
    ;
});
