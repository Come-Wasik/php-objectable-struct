<?php

use Nolikein\Objectable\Struct;

it('can create class for struct inheritence', function (): void {
    $inh = new class() extends Struct {
        protected array $constraints = [
            'myBoolean' => 'bool',
            'myString' => 'string',
        ];
    };

    $inh->fill([
        'myBoolean' => true,
        'myString' => 'text',
    ]);

    expect($inh)
        ->myBoolean->toBe(true)
        ->myString->toBe('text')
    ;
});
