<?php

use Nolikein\Objectable\Support\Arr;
it('can checks if an array is associative', function (): void {
    $assoc = ['a' => 1];
    $seq = [1, 2];

    expect(Arr::isAssoc($assoc))->toBeTrue();
    expect(Arr::isAssoc($seq))->toBeFalse();
});


it('can checks if an array is sequential', function (): void {
    $assoc = ['a' => 1];
    $seq = [1, 2];

    expect(Arr::isList($assoc))->toBeFalse();
    expect(Arr::isList($seq))->toBeTrue();
});
