<?php

use Nolikein\Objectable\Support\Str;

it('can check if string is contained', function (string $needle, string $haystack, bool $toBe): void {
    expect(Str::contains($needle, $haystack))
        ->toBe($toBe);
})->with([
    'present' => ['hello', '{hello}', true],
    'not present' => ['not hello', '{hello}', false],
]);

it('can retrieve string before', function (string $haystack, string $needle, string $toBe): void {
    expect(Str::before($needle, $haystack))
        ->toBe($toBe);
})->with([
    ['one:two', ':', 'one'],
    ['one:two', ':two', 'one'],
    [':one', ':', ''],
]);


it('can retrieve string after', function (string $haystack, string $needle, string $toBe): void {
    expect(Str::after($needle, $haystack))
        ->toBe($toBe);
})->with([
    ['one:two', ':', 'two'],
    ['one:two', 'one:', 'two'],
    ['one:', ':', ''],
]);


it('can check if starts with', function (string $haystack, string $needle, bool $toBe): void {
    expect(Str::startsWith($needle, $haystack))
        ->toBe($toBe);
})->with([
    [':one', ':', true],
    [':one', ':o', true],
    ['one:', ':', false],
]);
