<?php

it('can do action before a struct property is being updated', function (): void {
    newHookableStruct(['updatingAttribute' => true]);
})->throws(\RuntimeException::class, 'updatingAttribute');

it('can do action after a struct property is being updated', function (): void {
    newHookableStruct(['updatedAttribute' => true]);
    // expect()->
})->throws(\RuntimeException::class, 'updatedAttribute');

it('can do action before a struct constraint is being updated', function (): void {
    newHookableStruct([], ['updatingConstraint' => 'bool']);
})->throws(\RuntimeException::class, 'updatingConstraint');

it('can do action after a struct constraint is being updated', function (): void {
    newHookableStruct([], ['updatedConstraint' => 'bool']);
    // expect()->
})->throws(\RuntimeException::class, 'updatedConstraint');

it('can update total from struct automatically at creation', function () {
    $s = newPricingResultStruct([
        'price' => 5,
        'quantity' => 2,
    ]);

    expect($s->total)->toBe(10.0);
});

it('can update total from struct automatically after creation', function () {
    $s = newPricingResultStruct();
    $s->fill([
        'price' => 5,
        'quantity' => 2,
    ]);

    expect($s->total)->toBe(10.0);
});

it('can skip a value to be updated from a hook', function () {
    $s = newPricingResultStruct();
    $s->total = 10.0;

    expect($s->total)->toBeFloat()->toBe(0.0);
});

it('can pass a skip by disabling hooks', function () {
    $s = newPricingResultStruct();

    // Use basic anonymous function
    $s->withoutHooks(function ($s) {
        $s->total = 15.0;
    });
    expect($s->total)->toBeFloat()->toBe(15.0);

    // Use array function
    $s->withoutHooks(fn () => $s->total = 10.0);
    expect($s->total)->toBeFloat()->toBe(10.0);
});
